

#ifndef SNRFDOOYA_DIO_PIN
  #define SNRFDOOYA_DIO_PIN PIN_B7
#endif

#define SNRFDOOYA_Ppulse 65535 - (4830 * 5) 
#define SNRFDOOYA_Ppause 65535 - (1480 * 5) 
#define SNRFDOOYA_D0pulse 65535 - (320 * 5) 
#define SNRFDOOYA_D0pause 65535 - (730 * 5) 
#define SNRFDOOYA_D1pulse 65535 - (690 * 5) 
#define SNRFDOOYA_D1pause 65535 - (362 * 5)
#define SNRFDOOYA_PACdelay 65535 - (3700 * 5)

//  ========================== TX IR MODULE =========================
unsigned int8 SNRFDOOYA_tx_state = 0;
unsigned int8 SNRFDOOYA_tx_packet[5];
unsigned int8 SNRFDOOYA_tx_bit;
unsigned int8 SNRFDOOYA_tx_byte;
unsigned int8 SNRFDOOYA_tx_packet_rep;
int1 SNRFDOOYA_tx_curBit;
//int1 SNRFDOOYA_isINT_EXT_interupt;

void SNRFDOOYA_init();
void SNRFDOOYA_send(unsigned int32 addr, unsigned int8 cmd);

//  ==========================  =========================
void SNRFDOOYA_init() {
    output_drive(SNRFDOOYA_DIO_PIN);
    output_low(SNRFDOOYA_DIO_PIN); 
    setup_timer_3(T3_INTERNAL | T3_DIV_BY_1); //0,2us
    enable_interrupts(INT_TIMER3);
}

void SNRFDOOYA_send(unsigned int32 addr, unsigned int8 cmd) {
        SNRFDOOYA_tx_packet[0] = ((addr >> 24) & 0xFF);
        SNRFDOOYA_tx_packet[1] = ((addr >> 16) & 0xFF);
        SNRFDOOYA_tx_packet[2] = ((addr >> 8) & 0xFF);
        SNRFDOOYA_tx_packet[3] = (addr & 0xFF);       
        SNRFDOOYA_tx_packet[4] = cmd;
        SNRFDOOYA_tx_state = 1;
}

//  ========================== IR TRANSMIT MODULE =========================
#INT_TIMER3
void  TIMER3_isr(void) {
    switch (SNRFDOOYA_tx_state) {
    case 0:
        output_low(SNRFDOOYA_DIO_PIN);
        break;
    case 1:
        SNRFDOOYA_tx_packet_rep = 0;
        SNRFDOOYA_tx_state ++;
        break;        
    case 2:
        output_high(SNRFDOOYA_DIO_PIN);
        set_timer3(SNRFDOOYA_Ppulse); 
        SNRFDOOYA_tx_state ++;
        break;
    case 3:
        output_low(SNRFDOOYA_DIO_PIN);
        set_timer3(SNRFDOOYA_Ppause); //L-pause
        SNRFDOOYA_tx_state ++;
        SNRFDOOYA_tx_bit = 0;
        SNRFDOOYA_tx_byte = 0;
        break;
    case 4:
        if (SNRFDOOYA_tx_packet[SNRFDOOYA_tx_byte] & (0b10000000 >> SNRFDOOYA_tx_bit)) SNRFDOOYA_tx_curBit = 1; else SNRFDOOYA_tx_curBit = 0;
        output_high(SNRFDOOYA_DIO_PIN);
        if (SNRFDOOYA_tx_curBit) set_timer3(SNRFDOOYA_D1pulse); else set_timer3(SNRFDOOYA_D0pulse);
        SNRFDOOYA_tx_bit ++;
        if (SNRFDOOYA_tx_bit == 8) {SNRFDOOYA_tx_bit = 0; SNRFDOOYA_tx_byte ++;}
        SNRFDOOYA_tx_state ++;
        break;
    case 5:
        output_low(SNRFDOOYA_DIO_PIN);
        if (SNRFDOOYA_tx_curBit) set_timer3(SNRFDOOYA_D1pause); else set_timer3(SNRFDOOYA_D0pause);
        if (SNRFDOOYA_tx_byte < 5) SNRFDOOYA_tx_state = 4;
        else SNRFDOOYA_tx_state ++;
        break;
    case 6:
        set_timer3(SNRFDOOYA_PACdelay);
        SNRFDOOYA_tx_state ++;
        break;    
    case 7:
        SNRFDOOYA_tx_packet_rep ++;
        set_timer3(SNRFDOOYA_PACdelay);
        if (SNRFDOOYA_tx_packet_rep < 9) SNRFDOOYA_tx_state = 2;
        else SNRFDOOYA_tx_state ++;
        break; 
    case 8:
        output_low(SNRFDOOYA_DIO_PIN);
        //if (SNRFDOOYA_isINT_EXT_interupt) enable_interrupts(INT_EXT);
        set_timer3(0xFF00);
        SNRFDOOYA_tx_state = 0;
        break;
    }
}
