#include <main.h>
#include "dui.c"
#include "SNRFDOOYA.c"



//TASK MESSAGE CHECK
#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);
void readedcode(void);

//-------------------------------------------------

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x03 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);

    SNRFDOOYA_init();
    enable_interrupts(GLOBAL);
}

void task_DUIcheck() {
   DUICheck();
   restart_wdt();

}

void formInit(void) {
    DUIAddButton(0, (char*)"send 1");
    DUIAddButton(1, (char*)"send 2");
    DUIAddButton(2, (char*)"send 3");
    DUIAddButton(3, (char*)"Binding");
}

void formRead(unsigned int8 channel, unsigned int8* data) {
   switch(channel) {
      case 0: SNRFDOOYA_send(0x3127C101, 0x11); break;
      case 1: SNRFDOOYA_send(0x3127C101, 0x55); break;
      case 2: SNRFDOOYA_send(0x3127C101, 0x33); break;
      case 3: SNRFDOOYA_send(0x3127C101, 0xCC); break;
   }
}

void main()
{
   initialization();
   rtos_run();
}
